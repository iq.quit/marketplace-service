'use strict';


var app = require('../../server');
var request = require('supertest').agent(app.listen());

var expect = require('chai').expect;
var should = require('should');


describe('GET /products', function(){
  it('should respond with 200 type Array', function(done){
    request
    .get('/api/products')
    .expect(200, function(err, res) {
    	expect(Array.isArray(res.body)).to.be.true;
    	done();
    });
  });

  it('should respond with the best rate', function(done) {
    request
      .get('/api/products/best')
      .expect(200, function(err, res) {
        expect(res.body).to.be.a('object');
        expect(res.body.rate).to.equal(2.6299014);
        done();
      });
  });
});
