'use strict';

var controller = require('./products.controller');
var router = require('koa-router')();

router.get('/', controller.index);
router.get('/best', controller.bestRate);
module.exports = router.routes();
