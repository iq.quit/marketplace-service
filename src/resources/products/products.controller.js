'use strict';

var products = require('./products.json');

exports.index = function*(next) {
	this.status = 200;
  this.body = products;
};

exports.bestRate = function* (next) {
  this.status = 200;
  this.body = products.reduce(getBestRate, products[0]);

  function getBestRate(best, current) {
    return best.rate > current.rate ? best : current;
  }
};
